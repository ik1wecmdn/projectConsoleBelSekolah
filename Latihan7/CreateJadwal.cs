﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace Latihan7
{
    class CreateJadwal
    {
        public static void Create()
        {
            //Kotak(17, 5, 79, 26);
            Program.Kotak(18, 6, 78, 25, ConsoleColor.Black, ConsoleColor.Black);
            Program.Tulis(":: Input Data Jadwal                                     ", 
                20, 6, ConsoleColor.Black, ConsoleColor.Green);

            string hari = Program.InputString("Hari     : ", 
                20, 10, ConsoleColor.Green, ConsoleColor.Black);
            string jam = Program.InputString("Jam      : ", 
                20, 12, ConsoleColor.Green, ConsoleColor.Black);
            string keterangan = Program.InputString("Keterangan : ", 
                20, 14, ConsoleColor.Green, ConsoleColor.Black);
            string bel = Program.InputString("File Bel : ", 
                20, 16, ConsoleColor.Green, ConsoleColor.Black);


            
            //buat koneksi dulu
            MySqlConnection koneksi = new MySqlConnection("server=localhost;user=root;password=;port=3306;database=db_bel_sekolah");
            koneksi.Open();

            //tambahkan ke mysql menggunakan query
            string query = "INSERT INTO tb_jadwal (hari,jam,keterangan,nama_bel) VALUES (@hari,@jam,@keterangan,@nama_bel)";
            MySqlCommand cmd = koneksi.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@hari", hari);
            cmd.Parameters.AddWithValue("@jam", jam);
            cmd.Parameters.AddWithValue("@keterangan", keterangan);
            cmd.Parameters.AddWithValue("@nama_bel", bel);
            cmd.ExecuteNonQuery();

            //jangan lupa tutup koneksi
            koneksi.Close();

            Program.Kotak(18, 6, 78, 25, ConsoleColor.Black, ConsoleColor.Black);
        }
    }
}
