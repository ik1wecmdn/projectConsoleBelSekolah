﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latihan7
{
    class EditJadwal
    {
        public static void Edit()
        {
            Program.Kotak(18, 6, 78, 25, ConsoleColor.Black, ConsoleColor.Black);
            Program.Tulis(":: Edit Data Jadwal                                     ",
                20, 6, ConsoleColor.Black, ConsoleColor.Green);

            int idEdit = Program.InputInt("ID EDIT     : ",
               20, 10, ConsoleColor.Green, ConsoleColor.Black);

            //buat koneksi dulu
            MySqlConnection koneksi = new MySqlConnection("server=localhost;user=root;password=;port=3306;database=db_bel_sekolah");
            koneksi.Open();

            DataTable dtJadwal = new DataTable();
            string query = "SELECT * FROM tb_jadwal WHERE id=@id";
            MySqlCommand cmd = koneksi.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@id", idEdit);
            dtJadwal.Load(cmd.ExecuteReader());

            Program.Tulis("Hari       : " + dtJadwal.Rows[0]["hari"].ToString(), 20, 11, ConsoleColor.Cyan, ConsoleColor.Black);
            Program.Tulis("Jam        : " + dtJadwal.Rows[0]["jam"].ToString(), 20, 12, ConsoleColor.Cyan, ConsoleColor.Black);
            Program.Tulis("Keterangan : " + dtJadwal.Rows[0]["keterangan"].ToString(), 20, 13, ConsoleColor.Cyan, ConsoleColor.Black);
            Program.Tulis("Nama Bel   : " + dtJadwal.Rows[0]["nama_bel"].ToString(), 20, 14, ConsoleColor.Cyan, ConsoleColor.Black);

            //... lanjutkan sendiri 


        }
    }
}
