﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace Latihan7
{
    class Home
    {
        public static void Run()
        {
            Program.Kotak(18, 6, 78, 25, ConsoleColor.Black, ConsoleColor.Black);

            string[] hari = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" };
            string hariIni = hari[(int)DateTime.Now.DayOfWeek];

            bool selesai = false;
            do
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo tombol = Console.ReadKey(true);
                    if (tombol.Key == ConsoleKey.Escape)
                    {
                        selesai = true;
                    }
                }
                
                string jamSekarang = DateTime.Now.ToString("HH:mm:ss").Replace(".", ":");
                Program.Tulis(hariIni + " " + jamSekarang, 20, 8, ConsoleColor.Green, ConsoleColor.Black);

                MySqlConnection koneksi = new MySqlConnection("server=localhost;user=root;password=;port=3306;database=db_bel_sekolah");
                koneksi.Open();

                DataTable dtJadwal = new DataTable();
                string query = "SELECT * FROM tb_jadwal WHERE hari=@hariIni AND jam=@jamSekarang";
                MySqlCommand cmd = koneksi.CreateCommand();
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@hariIni", hariIni);
                cmd.Parameters.AddWithValue("@jamSekarang", jamSekarang);
                dtJadwal.Load(cmd.ExecuteReader());

                if (dtJadwal.Rows.Count > 0)
                {
                    SoundPlayer player = new SoundPlayer(dtJadwal.Rows[0]["nama_bel"].ToString());
                    player.Play();
                }
                System.Threading.Thread.Sleep(1000);
            }
            while (selesai == false);
        }
    }
}
