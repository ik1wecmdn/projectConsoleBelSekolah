﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latihan7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowWidth = 80;
            //--buat layout
            Kotak(0, 0, 79, 4);
            Tulis("Informatika 1", 35, 1, ConsoleColor.Yellow, ConsoleColor.Black);
            Tulis("Wearnes Education Center", 30, 2, ConsoleColor.Red, ConsoleColor.Black);
            Tulis("Madiun", 37, 3, ConsoleColor.Green, ConsoleColor.Black);

            Console.ForegroundColor = ConsoleColor.White;
            Kotak(0, 5, 16, 26);
            Kotak(17, 5, 79, 26);

            Kotak(0, 27, 79, 29);
            Tulis("developed with love by Tommy",28,28, ConsoleColor.Magenta, ConsoleColor.Black);
            //--end layout

            //--buat logo
            string[] logo= new string[12];
            logo[0]  = @"       _==/          i     i          \==_";
            logo[1]  = @"     /XX/            |\___/|            \XX\";
            logo[2]  = @"   /XXXX\            |XXXXX|            /XXXX\";
            logo[3]  = @"  |XXXXXX\_         _XXXXXXX_         _/XXXXXX|";
            logo[4]  = @" XXXXXXXXXXXxxxxxxxXXXXXXXXXXXxxxxxxxXXXXXXXXXXX";
            logo[5]  = @"|XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|";
            logo[6]  = @"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            logo[7]  = @"|XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|";
            logo[8]  = @" XXXXXX/^^^^^\XXXXXXXXXXXXXXXXXXXXX/^^^^^\XXXXXX";
            logo[9]  = @"  |XXX|       \XXX/^^\XXXXX/^^\XXX/       |XXX|";
            logo[10] = @"    \XX\       \X/    \XXX/    \X/       /XX/";
            logo[11] = @"        \       v      \X/      v       /";

            for (int i = 0; i <= 11; i++)
            {
                Tulis(logo[i], 24, 8+i, ConsoleColor.White, ConsoleColor.Black);
            }
            //--end logo


            //--buat menu

            string[] menu = new string[5];
            menu[0] = "    HOME       ";
            menu[1] = "    CREATE     ";
            menu[2] = "    EDIT       ";
            menu[3] = "    DELETE     ";
            menu[4] = "    SHOW       ";

            for (int i = 0; i <= 4; i++)
            {
                Tulis(menu[i], 1, 7+i*2, ConsoleColor.White, ConsoleColor.Black);
            }

            int terpilih = 0;
            Tulis(menu[terpilih], 1, 7 + terpilih * 2, ConsoleColor.Black, ConsoleColor.Green);

            ConsoleKeyInfo tombol;
            do
            {
                tombol = Console.ReadKey(true);
                if (tombol.Key == ConsoleKey.DownArrow)
                {
                    Tulis(menu[terpilih], 1, 7 + terpilih * 2, ConsoleColor.White, ConsoleColor.Black);
                    terpilih++;
                    if (terpilih == 5)
                    {
                        terpilih = 4;
                    }
                    Tulis(menu[terpilih], 1, 7 + terpilih * 2, ConsoleColor.Black, ConsoleColor.Green);

                }
                else if (tombol.Key == ConsoleKey.UpArrow)
                {
                    Tulis(menu[terpilih], 1, 7 + terpilih * 2, ConsoleColor.White, ConsoleColor.Black);
                    terpilih--;
                    if (terpilih == -1)
                    {
                        terpilih = 0;
                    }
                    Tulis(menu[terpilih], 1, 7 + terpilih * 2, ConsoleColor.Black, ConsoleColor.Green);
                }
                else if (tombol.Key == ConsoleKey.Enter)
                {
                    if (terpilih == 0)
                    {
                        Home.Run();
                    }
                    else if (terpilih == 1)
                    {
                        CreateJadwal.Create();
                    }
                    else if (terpilih == 2)
                    {
                        EditJadwal.Edit();
                    }
                    else if (terpilih == 3)
                    {
                        DeleteJadwal.Delete();
                    }
                    else if (terpilih == 4)
                    {
                        ShowJadwal.Show();
                    }

                }
            }
            while (tombol.Key != ConsoleKey.Escape);

            //--end menu


            //Console.ReadKey();
        }

        
        public static int InputInt(string label, int left, int top, ConsoleColor fg, ConsoleColor bg)
        {
            int hasilInput = 0;
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
            Console.SetCursorPosition(left, top);
            Console.Write(label);
            hasilInput = int.Parse(Console.ReadLine());

            return hasilInput;
        }

        public static string InputString(string label, int left, int top, ConsoleColor fg, ConsoleColor bg)
        {
            string hasilInput = "";
            do
            {
                Console.ForegroundColor = fg;
                Console.BackgroundColor = bg;
                Console.SetCursorPosition(left, top);
                Console.Write(label);
                hasilInput = Console.ReadLine();
            }
            while (hasilInput == "");
            return hasilInput;
        }

        public static void Kotak(int left, int top, int right, int bottom, ConsoleColor fg, ConsoleColor bg)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;

            Kotak(left, top, right, bottom);

            //arsiran tengah
            for (int i = top + 1; i <= bottom - 1; i++)
            {
                for (int j = left + 1; j <= right - 1; j++)
                {
                    Console.SetCursorPosition(j, i);
                    Console.WriteLine(" ");
                }
            }
        }

        public static void Kotak(int left, int top, int right, int bottom)
        {
            Console.SetCursorPosition(left, top);
            Console.Write("┌");   //218

            for (int i = left + 1; i <= right - 1; i++)
            {
                Console.SetCursorPosition(i, top);
                Console.Write("─");   //196
                //System.Threading.Thread.Sleep(10);
            }

            Console.SetCursorPosition(right, top);
            Console.Write("┐");  //191

            for (int i = top + 1; i <= bottom -1; i++)
            {
                Console.SetCursorPosition(right, i);
                Console.Write("│"); //179
                //System.Threading.Thread.Sleep(10);
            }

            Console.SetCursorPosition(right, bottom);
            Console.Write("┘");   //217

            for (int i = right - 1; i >= left + 1; i--)
            {
                Console.SetCursorPosition(i, bottom);
                Console.Write("─");   //196
                //System.Threading.Thread.Sleep(10);
            }

            Console.SetCursorPosition(left, bottom);
            Console.Write("└"); //192

            for (int i = bottom -1 ; i >= top + 1; i--)
            {
                Console.SetCursorPosition(left, i);
                Console.Write("│"); //179
                //System.Threading.Thread.Sleep(10);
            }
        }

        public static void Tulis(string teks, int left, int top, ConsoleColor fg, ConsoleColor bg)
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
            Console.SetCursorPosition(left, top);
            Console.WriteLine(teks);
        }

        
    }
}
