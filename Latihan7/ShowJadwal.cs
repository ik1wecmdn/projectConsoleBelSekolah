﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using MySql.Data.MySqlClient;

namespace Latihan7
{
    class ShowJadwal
    {
        public static void Show()
        {
            Program.Kotak(18, 6, 78, 25, ConsoleColor.Black, ConsoleColor.Black);

            string hari = Program.InputString("Hari yang ditampilkan : ", 20, 7, ConsoleColor.Green, ConsoleColor.Black);

            //buat koneksi
            MySqlConnection koneksi = new MySqlConnection("server=localhost;user=root;password=;port=3306;database=db_bel_sekolah");
            koneksi.Open();

            DataTable dtJadwal = new DataTable();
            string query = "SELECT * FROM tb_jadwal WHERE hari=@hari";
            MySqlCommand cmd = koneksi.CreateCommand();
            cmd.CommandText = query;
            cmd.Parameters.AddWithValue("@hari", hari);
            dtJadwal.Load(cmd.ExecuteReader());

            koneksi.Close();

            Program.Tulis("ID    HARI       JAM        KETERANGAN", 20, 8, ConsoleColor.Green, ConsoleColor.Black);
            Program.Tulis("======================================", 20, 9, ConsoleColor.DarkGreen, ConsoleColor.Black);

            for (int i = 0; i < dtJadwal.Rows.Count; i++)
            {
                Program.Tulis(dtJadwal.Rows[i]["id"].ToString(), 20, 10+i, ConsoleColor.Green, ConsoleColor.Black);
                Program.Tulis(dtJadwal.Rows[i]["hari"].ToString(), 26, 10 + i, ConsoleColor.Green, ConsoleColor.Black);
                Program.Tulis(dtJadwal.Rows[i]["jam"].ToString().Substring(0, 5), 37, 10 + i, ConsoleColor.Green, ConsoleColor.Black);
                Program.Tulis(dtJadwal.Rows[i]["keterangan"].ToString(), 48, 10 + i, ConsoleColor.Green, ConsoleColor.Black);
            }

        }
    }
}
